from django.urls import path  # type: ignore
from .views import show_my_tasks, complete_tasks
from projects.views import create_task

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:pk>/complete", complete_tasks, name="complete_tasks"),
]
