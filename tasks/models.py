from django.db import models  # type: ignore
from django.contrib.auth.models import User  # type: ignore
from projects.models import Project

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_complete = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, related_name="tasks", on_delete=models.CASCADE, null=True
    )
    assignee = models.ForeignKey(
        User, related_name="tasks", on_delete=models.SET_NULL, null=True
    )

    def __str__(self):
        return self.name
