from django.shortcuts import render, redirect, get_object_or_404  # type: ignore
from django.contrib.auth.decorators import login_required  # type: ignore
from tasks.models import Task
from projects.forms import CompleteTaskForm


@login_required
def show_my_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {"task_list": task}
    return render(request, "tasks/task_list.html", context)


def complete_tasks(request, pk):
    show_tasks = get_object_or_404(Task, pk=pk)
    if request.method == "POST":
        form = CompleteTaskForm(request.POST, instance=show_tasks)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = CompleteTaskForm(instance=show_tasks)
    return render(request, "tasks/task_list.html", {"form": form})
