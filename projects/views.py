from django.shortcuts import render, redirect  # type: ignore
from .models import Project  # type: ignore
from django.contrib.auth.decorators import login_required  # type: ignore
from tasks.models import Task
from .forms import ProjectForm, TaskForm

# Create your views here.


@login_required
def list_projects(request):
    project = Project.objects.filter(members=request.user)
    return render(request, "projects/list.html", {"project": project})


@login_required
def project_details(request, pk):
    tasks = Task.objects.filter(assignee=request.user)
    project = Project.objects.filter(members=request.user, pk=pk)
    context = {
        "tasks_list": tasks,
        "projects_list": project,
    }
    return render(request, "projects/tasks.html", context)


@login_required
def create_project(request):
    context = {}
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            new_project = form.save()
            return redirect("show_project", new_project.pk)
    return render(request, "projects/create.html", context)


@login_required
def create_task(request):
    context = {}
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            new_task = form.save()
            return redirect("show_project", new_task.pk)
        else:
            form = TaskForm
            context["form"] = form
    return render(request, "projects/create_task.html", context)
