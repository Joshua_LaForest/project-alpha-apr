# Generated by Django 4.0.6 on 2022-08-01 22:31

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("projects", "0003_rename_desciption_project_description_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="description",
            field=models.TextField(),
        ),
        migrations.AlterField(
            model_name="project",
            name="members",
            field=models.ManyToManyField(
                related_name="projects", to=settings.AUTH_USER_MODEL
            ),
        ),
    ]
