from django.urls import path  # type: ignore
from .views import list_projects, project_details, create_project  # type: ignore


urlpatterns = [
    path("", list_projects, name="home"),
    path("<int:pk>/", project_details, name="show_project"),
    path("create/", create_project, name="create_project"),
]
