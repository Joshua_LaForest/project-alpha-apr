
from django import forms
from .models import Project
from tasks.models import Task


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "members",
        ]


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = [
            "is_complete",
        ]


class CompleteTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = [
            "is_complete",
        ]
