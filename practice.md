## Feature 1
 * [x] Fork and clone the starter project from Project Alpha 
 * [x] Create a new virtual environment in the repository directory for the project
 * [x] Activate the virtual environment
 * [x] Upgrade pip
 * [x] Install django
 * [x] Install black
 * [x] Install flake8
 * [x] Install djhtml
 * [x] Deactivate your virtual environment
 * [x] Activate your virtual environment
 * [x] Use pip freeze to generate a requirements.txt file
 * [x] Test using ```python -m unittest tests.test_feature_01```
 * [x] commit/migrate

## Feature 2
 * [x] Create a Django project named tracker so that the manage.py file is in the top directory
 * [x] Create a Django app named accounts and install it in the tracker Django project in the INSTALLED_APPS list
 * [x] Create a Django app named projects and install it in the tracker Django project in the INSTALLED_APPS list
 * [x] Create a Django app named tasks and install it in the tracker Django project in the INSTALLED_APPS list
 * [x] Run the migrations
 * [x] Create a super user
 * [x] python manage.py test tests.test_feature_02
 * [x] commit/migrate

## feature 3
 * [x] in the projects django app create a model Project
 * [x] it will have 3 fields
 * [x] name, max length 200
 * [x] description no max length 
 * [x] members, many to many, auth.User, related name "projects"
 * [x] return self.name
 * [x] make migrations and test
 * [x] commit


## feature 4
 * [x] register the Project model in the admin file
 * [x] run test
 * [x] commit

## feature 5
 * [x] Create a view that will get all of the instances of the Project model and puts them in the context for the template
 * [x] Register that view in the projects app for the path "" and the name "list_projects" in a new file named projects/urls.py
 * [x] Include the URL patterns from the projects app in the tracker project with the prefix "projects/"
 * [x] Create a template for the list view that complies with the following specifications
 * [x] the fundamental five in it
 * [x] a main tag that contains
 * [x] div tag that contains
 * [x] an h1 tag with the content "My Projects"
 * [x] if there are no projects created, then
 * [x] a p tag with the content "You are not assigned to any projects"
 * [x] otherwise (else)
 * [x] a table that has two columns,
 * [x] The first with the header "Name" and the rows with the names of the projects
 * [x] The second with the header "Number of tasks" and nothing in those rows because we don't yet have tasks
  ## feature 6
 * [ ] In the tracker urls.py, use the RedirectView to redirect from "" to the name of path for the list view that you created in the previous feature. Register that path a name of "home".
## feature 7
 * [x] Register the LoginView  in your accounts urls.py with the path "login/" and the name "login"
 * [x] Include the URL patterns from the accounts app in the tracker project with the prefix "accounts/"
 * [x] Create a templates directory under accounts
 * [x] Create a registration directory under templates
 * [x] Create an HTML template named login.html in the registration directory
 * [x] Put a post form in the login.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
 * [x] In the tracker settings.py file, create and set the variable LOGIN_REDIRECT_URL to the value "home" which will redirect us to the path (not yet created) with the name "home"
 * [x] create an html template
 * [x] the fundamental five in it
 * [x] a main tag that contains
 * [x] div tag that contains
 * [x] an h1 tag with the content "Login"
 * [x] a form tag with method "post" that contains any kind of HTML structures but must include
 * [x] an input tag with type "text" and name "username"
 * [x] an input tag with type "password" and name "password"
 * [x] a button with the content "Login"
## feature 8
 * [x] Protect the list view for the Project model so that only a person that has logged in can  access it
* [x] Change the queryset of the view to filter the Project objects where members equals the logged in user
## feature 9
* [x] In the accounts/urls.py file,
* [x] Import the LogoutView from the same module that you imported the LoginView from
* [x] Register that view in your urlpatterns list with the path "logout/" and the name "logout"
* [x] In the tracker settings.py file, create and set the variable LOGOUT_REDIRECT_URL to the value "login" which will redirect the logout view to the login page
  ## feature 10
* [x] You'll need to import the UserCreationForm from the built-in auth forms 
* [x] You'll need to use the special create_user  method to create a new user account from their username and password
* [x] You'll need to use the login  function that logs an account in
* [x] After you have created the user, redirect the browser to the path registered with the name "home"
* [x] Create an HTML template named signup.html in the registration directory
* [x] Put a post form in the signup.html and any other required HTML or template inheritance stuff that you need to make it a valid Web page with the fundamental five (see specifications below)
* [x] the fundamental five in it
* [x] a main tag that contains
* [x] div tag that contains
* [x] an h1 tag with the content "Signup"
* [x] a form tag with method "post" that contains any kind of HTML structures but must include
* [x] an input tag with type "text" and name "username"
* [x] an input tag with type "password" and name "password1"
* [x] an input tag with type "password" and name "password2"
* [x] a button with the content "Signup"


This problem is big enough that I don't want to jump straight to a solution I need to create understanding first.

what don't I understand?
reasses the situation?
what is the veiw for?
we need this page to redirect.  the person needs to be able to immediatly see if they have tasks after creating their user account. 
1. create user account
2. redirect to task page

remember to stay within the scope of the problem, the task view is outside the scope, ignore it for now

redirect breakdown
redirect is given a value
redirect evaluates the value 

look for examples that are strict in this works this way. 
consider the validity of the website you are using, format, type of information, definitions, examples, specifications, analogies, 
when visiting a website understand the relevence of the source, format and type of information on a page we visit. predict the kind of information the site will offer. 
for django and python


hands off the keyboard


make a search
hands off keyboard
look thorugh the pulled up pages and read description to find most useful page, use the above instructions for that. 
if there is a subheading that is in line with the original search it is probably worth checking out. (the subheading specifically)

objective prediction
is it doing what I want?
If I am not accurately making predictions I need to step back and reasses my understanding of the topic.
understand it well enough to accurately predict how the code is going to behave.


look at error 
find line number
find what in that line is throwing the error
go  to line number
look at the variable/function that is throwing the error (by hovering over it) and identify if it is being used correctly or if it's values may be wrong or used incorrectly

## feature 11
* [x] create models Task
* [x] name string max length 200
* [x] start_date date-time no constraints
* [x] due_date, date-time no constraints
* [x] is_completed, bool, default = False
* [x] project, foreignkey, refers to projects.Project related name "tasks" on delete cascade
* [x] assignee, foreiogn key, refers to auth.User related name tasks on delete null, null = True
* [x] self.name
* [x] migrate

## Feature 12
* [ ] Register the Task model with the admin so that you can see it in the Django admin site.



## Feature 13
* [x] Create a view that shows the details of a particular project
* [x] A user must be logged in to see the view
* [x] In the projects urls.py file, register the view with the path "<int:pk>/" and the name "show_project"
* [x] Create a template to show the details of the project and a table of its tasks
* [x] Update the list template to show the number of tasks for a project
* [x] Update the list template to have a link from the project name to the detail view for that project
* [x] the fundamental five in it
* [x] a main tag that contains
* [x] div tag that contains
* [x] an h1 tag with the project's name as its content
* [x] a p tag with the project's description in it
* [x] an h2 tag that has the content "Tasks"
* [x] if the project has tasks, then
* [x] a table that contains five columns with the headers "Name", "Assignee", "Start date", "Due date", and "Is completed" with rows for each task in the project
* [x] otherwise
* [x] a p tag with the content "This project has no tasks"
  

  ## feature 14
* [x] Create a create view for the Project model that will show the name, description, and members properties in the form and handle the form submission to create a new Project
* [x] A person must be logged in to see the view
* [x] If the project is successfully created, it should redirect to the detail page for that project
* [x] Register that view for the path "create/" in the projects urls.py and the name "create_project"
* [x] Create an HTML template that shows the form to create a new * [ ] Project (see the template specifications below)
* [x] Add a link to the list view for the Project that navigates to the new create view
* [x] the fundamental five in it
* [x] a main tag that contains
* [x] div tag that contains
* [x] an h1 tag with the content "Create a new project"
* [x] a form tag with method "post" that contains any kind of HTML * [ ] structures but must include
* [ ] an input tag with type "text" and name "name"
* [ ] a textarea tag with the name "description"
* [ ] a select tag with name "members"
* [ ] a button with the content "Create"
  ## Feature 15
* [x] Create a view that will show a form to create an instance of * [ ] the Task model for all properties except the is_completed field
* [x] The view must only be accessible by people that are logged in
* [x] When the view successfully handles the form submission, have it redirect to the detail page of the task's project
* [x] Register that view in the tasks app for the path "create/" and the name "create_task" in a new file named tasks/urls.py
* [x] Include the URL patterns from the tasks app in the tracker project with the prefix "tasks/"
* [x] Create a template for the create view that complies with the following specifications
* [ ] Add a link to create a task from the project detail page that complies with the following specifications
* [x] the fundamental five in it
* [x] a main tag that contains
* [x] div tag that contains
* [x] an h1 tag with the content "Create a new task"
* [x] a form tag with method "post" that contains any kind of HTML * [ ] structures but must include
* [x] an input tag with type "text" and name "name"
* [x] an input tag with type "text" and name "start_date"
* [x] an input tag with type "text" and name "due_date"
* [x] a select tag with name "projects"
* [x] a select tag with name "assignee"
* [x] a button tag with the content "Create"
 ## feature 16 
* [x] Create a list view for the Task model with the objects filtered so that the person only sees the tasks assigned to them by filtering * [ ] with the assignee equal to the currently logged in user
The view must only be accessible by people that are logged in
* [x] Register that view in the tasks app for the path "mine/" and the name "show_my_tasks" in the tasks urls.py file
* [x] Create an HTML template that conforms with the following specification
* [x] the fundamental five in it
* [x] a main tag that contains
* [x] div tag that contains
* [x] an h1 tag with the content "My Tasks"
* [ ] if there are tasks assigned to the person
* [ ] a table with the headers "Name", "Start date", "End date", "Is * [ ] completed" and a row for each task that is assigned to the logged in person
* [ ] In the last column, if the task is completed, it should show the word "Done", otherwise, it should show nothing
* [ ] otherwise
* [ ] a p tag with the content "You have no tasks"
## feature 17
* [ ] Create an update view for the Task model that only is concerned with the is_completed field
* [ ] When the view successfully handles a submission, it should redirect to the "show_my_tasks" URL path, that is, it should redirect to the "My * [ ] Tasks" view (success_url property on a view class)
* [ ] Register that view in the tasks app for the path "<int:pk>/complete/" and the name "complete_task" in the tasks urls.py file
* [ ] You do not need to make a template for this view
* [ ] Modify the "My Tasks" view to comply with the template specification
* [ ] In the table cell that holds the status for each task in the "My Tasks" view, if the value of is_completed is False, then show this form.

<form method="post" action="{% url 'complete_task' task.id %}">
  {% csrf_token %}
  <input type="hidden" name="is_completed" value="True">
  <button>Complete</button>
</form>

## feature 18 
* [ ] Install the django-markdownify package using pip (the first pip command in the instructions) and put it into the INSTALLED_APPS in the tracker settings.py according to the Installation  instructions block

* [ ] Also, in the tracker settings.py file, add the configuration setting to disable sanitation 

* [ ] In your the template for the Project detail view, load the markdownify template library as shown in the Usage  section

* [ ] Replace the p tag and {{ project.description }} in the Project detail view with this code   {{ project.description|markdownify }}
* [ ] Use pip freeze to update your requirements.txt file
  
## Feature 19
* [ ] On all HTML pages, add

* [ ] a header tag as the first child of the body tag before the main tag that contains
* [ ] a nav tag that contains
* [ ] a ul tag that contains
* [ ] if the person is logged in,
* [ ] an li tag that contains
* [ ] an a tag with an href to the "show_my_tasks" path with the content "My tasks"
* [ ] an li tag that contains
* [ ] an a tag with an href to the "list_projects" path with the content "My projects"
* [ ] an li tag that contains
* [ ] an a tag with an href to the "logout" path with the content "Logout"
* [ ] otherwise
* [ ] an li tag that contains
* [ ] an a tag with an href to the "login" path with the content "Login"
* [ ] an li tag that contains
* [ ] an a tag with an href to the "signup" path with the content "Signup"
* [ ] t