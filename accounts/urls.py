from django.urls import path  # type: ignore
from .views import user_login, user_logout, signup  # type: ignore

# urlpatterns = [
#     path("login/", auth_views.LoginView.as_view(template_name = "registration/login.html")),
#     path("logout/", auth_views.LogoutView.as_view(template_name="registrationlogout.html")),
# ]

urlpatterns = [
    path("login/", user_login.as_view(), name="login"),
    path("logout/", user_logout.as_view(), name="logout"),
    path("signup/", signup, name="signup"),
]
